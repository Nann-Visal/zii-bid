<?php

namespace App\Models;

use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    public const ADMIN = 'Admin';
    public const CONSTRUCTOR = 'Construction';
    public const HOME_OWNER = 'HomeOwner';
    /**
     * @return array<string>
     */
    public static function allRoles(): array
    {
        return [
            self::ADMIN,
            self::CONSTRUCTOR,
            self::HOME_OWNER,
        ];
    }
}
