<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bid extends Model implements HasMedia
{
    use HasFactory,SoftDeletes,BlameableTrait, InteractsWithMedia;

    protected static $blameable = [
        'user' => User::class,
        'createdBy' => 'created_by',
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];

    protected $fillable = [
        'name',
        'size',
        'budget',
        'description',
        'address',
        'expired_at',
        'status',
        'types',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $casts = [
        'size' => 'double',
        'budget' => 'double',
        'status' => 'integer',
        'types' => 'integer',
        'expired_at' => 'timestamp',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id')->withTimestamps();
    }

    public function bidItems() 
    {
        return $this->belongsToMany(BidItem::class);
    }

     public function registerMediaCollections(): void
    {
        $this->addMediaCollection('thumbnail')
            ->acceptsMimeTypes([
                'image/jpeg',
                'image/png',
                'image/svg+xml',
                'image/webp',
                'image/gif',
                'image/svg',
            ])
            ->singleFile();
        $this->addMediaCollection('document');
    }
}
