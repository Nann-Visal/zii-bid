<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Message extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, InteractsWithMedia, BlameableTrait;

    protected static $blameable = [
        'user' => User::class,
        'createdBy' => 'sender_id',
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];

    protected $fillable = [
        'content',
        'type',
        'group_id',
        'sender_id',
        'updated_by',
        'deleted_by',
    ];

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('preview')
            ->width(368)
            ->height(232)
            ->sharpen(10)
            ->nonOptimized();
    }
}
