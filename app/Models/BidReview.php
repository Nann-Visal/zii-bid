<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BidReview extends Model
{
    use HasFactory, SoftDeletes, BlameableTrait;

    protected static $blameable = [
        'user' => User::class,
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];

    protected $fillable = [
        'bid_id',
        'bid_item_id',
        'text_note',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function bid()
    {
        return $this->belongsTo(Bid::class, 'bid_id');
    }

    public function bidItem()
    {
        return $this->belongsTo(BidItem::class, 'bid_item_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
