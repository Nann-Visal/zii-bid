<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Filament\Models\Contracts\FilamentUser;
use Filament\Panel;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia, MustVerifyEmail, FilamentUser
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia, HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'avatar',
        'address',
        'billing_address',
        'date_of_birth',
        'types',
        'status'
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function bids() : HasMany
    {
        return $this->hasMany(Bid::class);
    }


    public function getUserTypes()
    {
        return $this->types;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatars')
            ->acceptsMimeTypes(
                [
                    'image/jpeg',
                    'image/png',
                    'image/svg+xml',
                    'image/webp',
                    'image/gif',
                    'image/svg',
                ]
            )
            ->singleFile();;
    }

    public function canAccessPanel(Panel $panel): bool
    { 
        if ($panel->getId() === 'admin') {
            return str_ends_with($this->email, 'visalnann@gmail.com') && $this->hasVerifiedEmail();
        } 
        return true;
    }
}
