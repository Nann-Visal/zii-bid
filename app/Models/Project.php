<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasFactory, BlameableTrait, SoftDeletes;

    protected static $blameable = [
        'user' => User::class,
        'createdBy' => 'created_by',
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];


    protected $fillable = [
        'start_date',
        'end_date',
        'budget',
        'location',
        'description',
        'bid_review_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $casts = [
        'budget' => 'double',
        'size' => 'double',
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function bidReviewed()
    {
        return $this->belongsTo(BidReview::class, 'bid_review_id');
    }

    public function tasks() {
        return $this->hasMany(Task::class);
    }
}
