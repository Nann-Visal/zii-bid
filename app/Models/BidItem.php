<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BidItem extends Model
{
    use HasFactory, SoftDeletes, BlameableTrait;

    protected static $blameable = [
        'user' => User::class,
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];

    protected $fillable = [
        'bid_id',
        'bid_taxes',
        'description',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $casts = [
        'bid_taxes' => 'double',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function bid()
    {
        return $this->belongsTo(Bid::class, 'bid_id');
    }
}
