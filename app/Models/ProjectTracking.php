<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectTracking extends Model
{
    use HasFactory, BlameableTrait, SoftDeletes;

    protected static $blameable = [
        'user' => User::class,
        'createdBy' => 'created_by',
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];


    protected $fillable = [
        'total_invest',
        'total_income',
        'note',
        'status',
        'project_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $casts = [
        'total_invest' => 'double',
        'total_income' => 'double',
        'status' => 'integer',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
