<?php

namespace App\Models;

use App\Models\User;
use App\Models\BidReview;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Group extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, BlameableTrait, InteractsWithMedia;

    protected static $blameable = [
        'user' => User::class,
        'updatedBy' => 'updated_by',
        'deletedBy' => 'deleted_by',
    ];

    protected $fillable = [
        'name',
        'slug',
        'status',
        'bid_review_id',
        'created_by',
    ];

    protected $casts = [
        'status' => 'integer',
    ];

    /**
     * Register media file for this mode;
     *
     * @param Media|null $media un document
     * 
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('preview')
            ->width(368)
            ->height(232)
            ->sharpen(10)
            ->nonOptimized();
    }

    /**
     * Relation to BidReview
     *
     * @return void
     */
    public function bidReview()
    {
        return $this->belongsTo(BidReview::class);
    }

    /**
     * Relation to User 
     *
     * @return void
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
