<?php

namespace App\Enums\Message;

use BenSampo\Enum\Enum;

class CollectionName extends Enum
{
    const FILE = 'file';
    const TEXT = 'text';
    const VOICE = 'voice';
}
