<?php

namespace App\Enums\ProjectTracking;

use App\Enums\BaseEnum;

class Status extends BaseEnum
{
    const NEW_ARRIVAL = 0;
    const IN_PROGRESS = 1;
    const IN_REVIEW = 2;
    const FAILS = 3;
    const DONE = 4; 
}
