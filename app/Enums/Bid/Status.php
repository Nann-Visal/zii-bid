<?php

namespace App\Enums\Bid;

use App\Enums\BaseEnum;



final class Status extends BaseEnum
{
    const OPEN = 1;
    const CLOSE = 0;
}
