<?php

namespace App\Enums\User;

use App\Enums\Status as BaseStatus;

final class Types extends BaseStatus
{
    const CONSTRUCTOR = 0;
    const HOMEOWNER = 1;
}
