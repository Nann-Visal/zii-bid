<?php

namespace App\Enums\Task;

use App\Enums\BaseEnum;

class Status extends BaseEnum
{
    const TO_DO = 0;
    const IN_PROGRESS = 1;
    const IN_PROBLEM = 2;
    const IN_REVIEW = 3;
    const DONE = 4;
}
