<?php

namespace App\Enums\Project;

use App\Enums\BaseEnum;

class Status extends BaseEnum
{
    const INACTIVE = 0;
    const ACTIVE = 1;
}
