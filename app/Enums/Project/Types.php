<?php

namespace App\Enums\Project;

use App\Enums\BaseEnum;



final class Types extends BaseEnum
{
    const REMODEL = 0;
    const CREATENEW = 1;
    const PARTOF = 2;
}
