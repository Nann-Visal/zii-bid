<?php

namespace App\Enums\NotificationUser;

use App\Enums\BaseEnum;

class Status extends BaseEnum
{
    const DRAFT = 0;
    const SENT = 1;
    const FAIL = 2;
}
