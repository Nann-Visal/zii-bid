<?php

namespace App\Enums;

use App\Enums\BaseEnum;

class Status extends BaseEnum
{
    const Inactive = 0;
    const Active = 1;
}
