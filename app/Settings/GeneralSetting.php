<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class GeneralSetting extends Settings
{
    public ?string $about_us;

    public ?string $privacy_policy;

    public ?array $languages;

    public ?string $copy_right_text;

    public ?array $social_medias;

    public ?string $company_name;

    public ?string $phone;

    public ?string $email;

    public ?string $company_address;

    public static function group(): string
    {
        return 'general';
    }
}