<?php

namespace App\Filament\Resources;

use Filament\Tables;
use App\Models\Project;
use Filament\Forms\Form;
use Filament\Tables\Table;
use App\Enums\Project\Types;
use Filament\Resources\Resource;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Notifications\Notification;
use Filament\Forms\Components\DatePicker;
use Filament\Tables\Columns\ToggleColumn;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\ProjectResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ProjectResource extends Resource
{
    protected static ?string $model = Project::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-carbon-for-ibm-product'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_project'); // Individual resource label
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_project_title'))
                        ->schema(
                            [
                                TextInput::make('budget')
                                    ->label(__('form_project_budget'))
                                    ->required()
                                    ->numeric(),
                                TextInput::make('size')
                                    ->label(__('form_project_size'))
                                    ->required()
                                    ->numeric(),
                                DatePicker::make('start_date')
                                    ->required()
                                    ->label(__('form_project_start_date'))
                                    ->live()
                                    ->placeholder('dd-mm-YY')
                                    ->displayFormat('d-m-Y')
                                    ->before('end_date')
                                    ->closeOnDateSelection()
                                    ->native(false),
                                DatePicker::make('end_date')
                                    ->required()
                                    ->label(__('form_project_end_date'))
                                    ->placeholder('dd-mm-YY')
                                    ->displayFormat('d-m-Y')
                                    ->after('start_date')
                                    ->closeOnDateSelection()
                                    ->native(false),
                                TextInput::make('location')
                                    ->label(__('form_project_location'))
                                    ->required()
                                    ->columnSpan(2),
                                Section::make()
                                    ->columnSpan(2)
                                    ->schema(
                                        [
                                            Textarea::make('description')
                                                ->label(__('form_project_description'))
                                                ->required()
                                                ->placeholder(__('form_project_description').'. . .')
                                                ->rows(3),
                                        ]
                                    ),
                                Section::make()
                                    ->columnSpan(1)
                                    ->schema(
                                        [
                                            Select::make('types')
                                                ->options(
                                                    [
                                                        Types::REMODEL => __('form_project_type_remodel'),
                                                        Types::CREATENEW => __('form_project_type_new'),
                                                        Types::PARTOF => __('form_project_type_part_of'),
                                                    ]
                                                )
                                                ->searchable()
                                                ->required()
                                                ->label(__('form_project_type'))
                                                ->native(false),
                                            Toggle::make('status')
                                                ->label(__('form_project_status'))
                                                ->default(false)
                                                ->inline(false),
                                        ]
                                    )
                            ]
                        )->columns(3)
                ]
            );
    }

    public static function table(Table $table): Table
    {
        return $table
            ->paginated([10, 25, 50, 100, 'all'])
            ->columns(
                [
                    TextColumn::make('bidReviewed.creator.name')
                        ->label(__('table_project_owner'))
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('bidReviewed.bidItem.creator.name')
                        ->label(__('table_project_bidder'))
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('budget')
                        ->label(__('table_project_budget'))
                        ->sortable()
                        ->searchable()
                        ->getStateUsing(
                            function (Project $record) {
                                return $record->budget.' $$';
                            }
                        ),
                    TextColumn::make('size')
                        ->label(__('table_project_size'))
                        ->sortable()
                        ->searchable()
                        ->getStateUsing(
                            function (Project $record) {
                                return $record->size.' m^2/m^3';
                            }
                        ),
                    ToggleColumn::make('status')
                        ->label(__('table_project_status'))
                        ->afterStateUpdated(
                            function () {
                                Notification::make()
                                    ->title(__('notification_edit'))
                                    ->success() 
                                    ->send();
                            }
                        )
                        ->onColor('success')
                        ->sortable(),
                    TextColumn::make('types')
                        ->label(__('table_project_type'))
                        ->getStateUsing(
                            function (Project $record): string {
                                $type = $record->types;
                                switch ($type) {
                                case Types::REMODEL : 
                                    return __('table_project_type_remodel');
                                case Types::CREATENEW :
                                    return __('table_project_type_new');
                                case Types::PARTOF :
                                    return __('table_project_type_part_of');
                                }
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_project_type_remodel') => 'gray',
                                __('table_project_type_new') => 'warning',
                                __('table_project_type_part_of') => 'success',
                            }
                        )
                        ->sortable(),
                    TextColumn::make('location')
                        ->label(__('table_project_location'))
                        ->sortable()
                        ->searchable()
                        ->limit(50),
                    TextColumn::make('start_date')
                        ->label(__('table_project_start_date'))
                        ->sortable()
                        ->searchable()
                        ->getStateUsing(
                            function (Project $record) {
                                return $record->start_date ?? __('table_project_no_record');
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_project_no_record') => 'warning',
                                default => 'success',
                            }
                        ),
                    TextColumn::make('end_date')
                        ->label(__('table_project_end_date'))
                        ->sortable()
                        ->searchable()
                        ->getStateUsing(
                            function (Project $record) {
                                return $record->end_date ?? __('table_project_no_record');
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_project_no_record') => 'warning',
                                default => 'primary',
                            }
                        ),
                    
                ]
            )
            ->filters(
                [
                    Tables\Filters\TrashedFilter::make(),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                    Tables\Actions\DeleteAction::make()
                        ->label(__('button_delete'))
                        ->modalCancelActionLabel(__('button_cancel'))
                        ->modalHeading(__('button_delete_header'))
                        ->modalDescription(__('button_delete_description'))
                        ->modalSubmitActionLabel(__('button_delete_confirm'))
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                        Tables\Actions\DeleteBulkAction::make(),
                        Tables\Actions\ForceDeleteBulkAction::make(),
                        Tables\Actions\RestoreBulkAction::make(),
                        ]
                    ),
                ]
            );
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProjects::route('/'),
            'create' => Pages\CreateProject::route('/create'),
            'edit' => Pages\EditProject::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
