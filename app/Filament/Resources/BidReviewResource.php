<?php

namespace App\Filament\Resources;

use App\Models\Bid;
use Filament\Tables;
use App\Models\BidItem;
use Filament\Forms\Get;
use Filament\Forms\Form;
use App\Models\BidReview;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Illuminate\Support\Collection;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\BidReviewResource\Pages;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class BidReviewResource extends Resource
{
    protected static ?string $model = BidReview::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-model-builder'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_bid_review'); // Individual resource label
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_bid_review_title'))
                        ->schema(
                            [
                                Select::make('bid_id')
                                    ->label(__('form_bid_review_bid'))
                                    ->required()
                                    ->options(Bid::query()->pluck('name', 'id'))
                                    ->live()
                                    ->searchable()
                                    ->preload(),
                                Select::make('bid_item_id')
                                    ->label(__('form_bid_review_bidder'))
                                    ->options(
                                        fn (Get $get): Collection => BidItem::query()
                                            ->where('bid_id', $get('bid_id'))
                                            ->with('creator')
                                            ->get()
                                            ->map(
                                                fn (BidItem $item) => [
                                                    'label' => $item->creator ? $item->creator->name : 'No record!',
                                                    'value' => $item->id,
                                                ]
                                            )
                                            ->pluck('label', 'value')
                                    )
                                    ->searchable()
                                    ->preload(),
                                Textarea::make('text_note')
                                    ->label(__('form_bid_review_note'))
                                    ->placeholder(__('form_bid_review_note').'. . .')
                                    ->rows(3)
                                    ->columnStart(1)
                                    ->columnSpan(2)
                                    ->required()

                                    
                            ]
                        )->columns(2)
                ]
            );
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns(
                [
                    TextColumn::make('creator.name')
                        ->label(__('table_bid_review_owner'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('bidItem.creator.name')
                        ->label(__('table_bid_review_bidder'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('bid.name')
                        ->label(__('table_bid_review_title'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('bid.budget')
                        ->label(__('table_bid_review_budget'))
                        ->badge()
                        ->color('primary')
                        ->getStateUsing(
                            function ($record) {
                                return $record->bid->budget . ' $$';
                            }
                        )
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('bidItem.bid_taxes')
                        ->label(__('table_bid_review_taxes'))
                        ->badge()
                        ->color('success')
                        ->getStateUsing(
                            function ($record) {
                                return $record->bidItem->bid_taxes.' $$';
                            }
                        )
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('text_note')
                        ->label(__('table_bid_review_note'))
                        ->searchable()
                        ->sortable(),
                ]
            )
            ->filters(
                [
                    Tables\Filters\TrashedFilter::make(),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                    Tables\Actions\DeleteAction::make()
                        ->label(__('button_delete'))
                        ->modalCancelActionLabel(__('button_cancel'))
                        ->modalHeading(__('button_delete_header'))
                        ->modalDescription(__('button_delete_description'))
                        ->modalSubmitActionLabel(__('button_delete_confirm'))
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                            Tables\Actions\DeleteBulkAction::make(),
                            Tables\Actions\ForceDeleteBulkAction::make(),
                            Tables\Actions\RestoreBulkAction::make(),
                        ]
                    ),
                ]
            );
    }

    public static function getRelations(): array
    {
        return [
           
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBidReviews::route('/'),
            'create' => Pages\CreateBidReview::route('/create'),
            'edit' => Pages\EditBidReview::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
