<?php
/**
 * Undcumented
 * 
 * @category BidItem
 * @package  Resource
 * @author   Zii <email@email.com>
 * @license  Zii 
 * @link     http://url.com
 */
namespace App\Filament\Resources;

use App\Filament\Resources\BidItemResource\Pages;
use App\Models\Bid;
use App\Models\BidItem;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

/**
 * Undocumented class
 * 
 * @category BidItem
 * @package  Resource
 * @author   Zii <email@email.com>
 * @license  Zii 
 * @link     http://url.com
 */
class BidItemResource extends Resource
{
    protected static ?string $model = BidItem::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'heroicon-m-adjustments-horizontal'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_bid_track'); // Individual resource label
    }

    /**
     * Undocumented function
     *
     * @param Form $form user request
     * 
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_bid_tracking_title'))
                        ->schema(
                            [
                                Select::make('bid_id')
                                    ->label(__('form_bid_tracking_bid'))
                                    ->required()
                                    ->relationship(name: 'bid', titleAttribute: 'name')
                                    ->searchable()
                                    ->preload(),
                                TextInput::make('bid_taxes')
                                    ->label(__('form_bid_tracking_tax'))
                                    ->placeholder('000.00')
                                    ->numeric()
                                    ->required(),
                                Textarea::make('description')
                                    ->label(__('form_bid_tracking_description'))
                                    ->placeholder(__('form_bid_tracking_description').'. . .')
                                    ->required()
                                    ->rows(3)
                                    ->columnSpan(3)
                            ]
                        )->columns(3)
                ]
            );
    }

    /**
     * Undocumented function
     *
     * @param Table $table display list
     * 
     * @return Table
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns(
                [
                    TextColumn::make('creator.name')
                        ->label(__('table_bid_tracking_creator'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('bid_taxes')
                        ->getStateUsing(
                            function ($record) {
                                return $record->bid_taxes.' $$';
                            }
                        )
                        ->label(__('table_bid_tracking_tax'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('bid.name')
                        ->label(__('table_bid_tracking_bid'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('description')
                        ->label(__('table_bid_tracking_description'))
                        ->limit(70)
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('created_at')
                        ->label(__('table_bid_tracking_created_at'))
                        ->badge()
                        ->color('success')
                        ->searchable()
                        ->sortable(),
                ]
            )
            ->filters(
                [
                    Tables\Filters\TrashedFilter::make(),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                    Tables\Actions\DeleteAction::make()
                        ->label(__('button_delete'))
                        ->modalCancelActionLabel(__('button_cancel'))
                        ->modalHeading(__('button_delete_header'))
                        ->modalDescription(__('button_delete_description'))
                        ->modalSubmitActionLabel(__('button_delete_confirm'))
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                            Tables\Actions\DeleteBulkAction::make(),
                            Tables\Actions\ForceDeleteBulkAction::make(),
                            Tables\Actions\RestoreBulkAction::make(),
                        ]
                    ),
                ]
            );
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBidItems::route('/'),
            'create' => Pages\CreateBidItem::route('/create'),
            'edit' => Pages\EditBidItem::route('/{record}/edit'),
        ];
    }

    /**
     * Undocumented function
     *
     * @return Builder
     */
    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes(
                [
                    SoftDeletingScope::class,
                ]
            );
    }
}
