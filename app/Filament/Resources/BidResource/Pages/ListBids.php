<?php

namespace App\Filament\Resources\BidResource\Pages;

use Filament\Actions;
use App\Filament\Resources\BidResource;
use Filament\Resources\Pages\ListRecords;

class ListBids extends ListRecords
{
    
    protected static string $resource = BidResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->label(__('button_create')),
        ];
    }
}
