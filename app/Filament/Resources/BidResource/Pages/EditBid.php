<?php

namespace App\Filament\Resources\BidResource\Pages;

use Filament\Actions\Action;
use App\Filament\Resources\BidResource;
use Filament\Resources\Pages\EditRecord;

class EditBid extends EditRecord
{

    protected static string $resource = BidResource::class;

    public function getTitle(): string 
    {
        return __('_form_title_edit');
    }

    protected function getSaveFormAction(): Action
    {
        return parent::getSaveFormAction()
            ->label(__('button_save_edit'));
    }

    protected function getCancelFormAction(): Action
    {
        return parent::getCancelFormAction()
            ->label(__('button_cancel'));
    }

    protected function getRedirectUrl(): ?string
    {
        return static::getResource()::getUrl('index');
    }
}
