<?php

namespace App\Filament\Resources;

use Althinect\FilamentSpatieRolesPermissions\Resources\RoleResource as ResourcesRoleResource;

class RoleResource extends ResourcesRoleResource
{

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-ibm-cloud-kubernetes-service'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getPluralLabel(): string
    {
        return __('nav_title_role'); // Individual resource label
    }
}
