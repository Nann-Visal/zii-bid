<?php

namespace App\Filament\Resources;

use Althinect\FilamentSpatieRolesPermissions\Resources\PermissionResource as ResourcesPermissionResource;

class PermissionResource extends ResourcesPermissionResource
{

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-ibm-cloud-app-id'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getPluralLabel(): string
    {
        return __('nav_title_permission'); // Individual resource label
    }
}
