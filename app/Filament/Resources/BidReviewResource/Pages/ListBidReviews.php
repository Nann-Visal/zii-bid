<?php

namespace App\Filament\Resources\BidReviewResource\Pages;

use App\Filament\Resources\BidReviewResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListBidReviews extends ListRecords
{
    protected static string $resource = BidReviewResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->label(__('button_create')),
        ];
    }
}
