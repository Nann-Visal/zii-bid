<?php

namespace App\Filament\Resources\BidReviewResource\Pages;

use Filament\Actions;
use Filament\Actions\Action;
use Filament\Resources\Pages\EditRecord;
use App\Filament\Resources\BidReviewResource;

class EditBidReview extends EditRecord
{
    protected static string $resource = BidReviewResource::class;

    public function getTitle(): string 
    {
        return __('_form_title_edit');
    }

    protected function getSaveFormAction(): Action
    {
        return parent::getSaveFormAction()
            ->label(__('button_save_edit'));
    }

    protected function getCancelFormAction(): Action
    {
        return parent::getCancelFormAction()
            ->label(__('button_cancel'));
    }
    
    protected function getRedirectUrl(): ?string
    {
        return static::getResource()::getUrl('index');
    }
}
