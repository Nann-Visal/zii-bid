<?php

namespace App\Filament\Resources;

use App\Models\Task;
use Filament\Tables;
use App\Models\Project;
use Filament\Forms\Form;
use App\Enums\Task\Status;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Illuminate\Support\Collection;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\DatePicker;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;

use App\Filament\Resources\TaskResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class TaskResource extends Resource
{
    protected static ?string $model = Task::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-load-balancer-vpc'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_task'); // Individual resource label
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_task_title'))
                        ->schema(
                            [
                                Section::make()
                                    ->schema(
                                        [
                                            Select::make('project_id')
                                                ->label(__('form_task_project'))
                                                ->required()
                                                ->options(
                                                    fn (): Collection => Project::query()
                                                        ->get()
                                                        ->map(
                                                            fn (Project $item) => [
                                                                'label' => $item->bidReviewed->bid ? $item->bidReviewed->bid->name : __('form_task_no_record'),
                                                                'value' => $item->id,
                                                            ]
                                                        )
                                                        ->pluck('label', 'value')
                                                )
                                                ->native(false)
                                                ->searchable()
                                                ->preload(),
                                            TextInput::make('name')
                                                ->label(__('form_task_name'))
                                                ->required()
                                                ->placeholder(__('form_task_name').'. . .'),
                                            DatePicker::make('start_date')
                                                ->required()
                                                ->label(__('form_task_start_date'))
                                                ->live()
                                                ->placeholder('dd-mm-YY')
                                                ->displayFormat('d-m-Y')
                                                ->before('end_date')
                                                ->closeOnDateSelection()
                                                ->native(false),
                                            DatePicker::make('end_date')
                                                ->required()
                                                ->label(__('form_task_end_date'))
                                                ->placeholder('dd-mm-YY')
                                                ->displayFormat('d-m-Y')
                                                ->after('start_date')
                                                ->closeOnDateSelection()
                                                ->native(false),
                                        ]
                                    )->columns(4),
                                Section::make()
                                    ->schema(
                                        [
                                            Select::make('status')
                                                ->options(
                                                    [
                                                        Status::TO_DO => __('form_task_status_todo'),
                                                        Status::IN_PROGRESS => __('form_task_status_in_progress'),
                                                        Status::IN_PROBLEM => __('form_task_status_in_problem'),
                                                        Status::IN_REVIEW => __('form_task_status_in_review'),
                                                        Status::DONE => __('form_task_status_done'),
                                                    ]
                                                )
                                                ->searchable()
                                                ->required()
                                                ->label(__('form_task_status'))
                                                ->native(false),
                                            Textarea::make('description')
                                                ->required()
                                                ->label(__('form_task_description'))
                                                ->placeholder(__('form_task_description').'. . .')
                                                ->rows(3)
                                                ->columnSpan(2)
                                        ]
                                    )->columns(3)
                            ]
                        )
                ]
            );
    }

    public static function table(Table $table): Table
    {
        return $table
            ->paginated([10, 25, 50, 100, 'all'])
            ->columns(
                [
                    TextColumn::make('project.bidReviewed.bid.name')
                        ->label(__('table_task_project'))
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('name')
                        ->label(__('table_task_name'))
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('status')
                        ->label(__('table_task_status'))
                        ->getStateUsing(
                            function (Task $record): string {
                                $status = $record->status;
                                switch ($status) {
                                case Status::TO_DO : 
                                    return __('table_task_status_todo');
                                case Status::IN_PROGRESS :
                                    return __('table_task_status_in_progress');
                                case Status::IN_PROBLEM :
                                    return __('table_task_status_in_problem');
                                case Status::IN_REVIEW :
                                    return __('table_task_status_in_review');
                                case Status::DONE :
                                    return __('table_task_status_done');
                                }
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_task_status_todo') => 'gray',
                                __('table_task_status_in_progress') => 'info',
                                __('table_task_status_in_problem') => 'danger',
                                __('table_task_status_in_review') => 'primary',
                                __('table_task_status_done')  => 'success',
                            }
                        )
                        ->sortable(),
                    TextColumn::make('start_date')
                        ->label(__('table_task_start_date'))
                        ->sortable()
                        ->searchable()
                        ->getStateUsing(
                            function (Task $record) {
                                return $record->start_date ?? __('table_task_no_record');
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_task_no_record') => 'warning',
                                default => 'success',
                            }
                        ),
                    TextColumn::make('end_date')
                        ->label(__('table_task_end_date'))
                        ->sortable()
                        ->searchable()
                        ->getStateUsing(
                            function (Task $record) {
                                return $record->end_date ?? __('table_task_no_record');
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_task_no_record') => 'warning',
                                default => 'primary',
                            }
                        ),
                    TextColumn::make('description')
                        ->label('table_task_description')
                        ->searchable()
                        ->sortable()
                        ->limit(75)
                ]
            )
            ->filters(
                [
                    Tables\Filters\TrashedFilter::make(),
                    
                    SelectFilter::make('project_id')
                        ->options(
                            fn (): Collection => Project::query()
                                ->get()
                                ->map(
                                    fn (Project $item) => [
                                        'label' => $item->bidReviewed->bid ? $item->bidReviewed->bid->name : 'No record!',
                                        'value' => $item->id,
                                    ]
                                )
                                ->pluck('label', 'value')
                        )
                        ->multiple()
                        ->native(false)
                        ->searchable()
                        ->preload(),
                    SelectFilter::make('status')
                        ->options(
                            [
                                Status::TO_DO => 'To Do',
                                Status::IN_PROGRESS => 'In Progress',
                                Status::IN_PROBLEM => 'In Problem',
                                Status::IN_REVIEW => 'In Review',
                                Status::DONE => 'Done',
                            ]
                        )
                        ->multiple()
                        ->searchable()
                        ->label('Status')
                        ->native(false),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                    Tables\Actions\DeleteAction::make()
                        ->label(__('button_delete'))
                        ->modalCancelActionLabel(__('button_cancel'))
                        ->modalHeading(__('button_delete_header'))
                        ->modalDescription(__('button_delete_description'))
                        ->modalSubmitActionLabel(__('button_delete_confirm'))
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                            Tables\Actions\DeleteBulkAction::make(),
                            Tables\Actions\ForceDeleteBulkAction::make(),
                            Tables\Actions\RestoreBulkAction::make(),
                        ]
                    ),
                ]
            );
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTasks::route('/'),
            'create' => Pages\CreateTask::route('/create'),
            'edit' => Pages\EditTask::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
