<?php

/**
 * This is bid feature 
 * 
 * @category Class
 * @package  Bid_Resource
 * @author   Nann Visal <visalnann@email.com>
 * @license  Zii23 Platform 
 * @link     http://
 */

namespace App\Filament\Resources;

use App\Models\Bid;
use Filament\Tables;
use App\Enums\Bid\Types;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Notifications\Notification;
use Filament\Tables\Columns\ToggleColumn;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\BidResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;

/**
 * This is bid feature 
 * 
 * @category Class
 * @package  Bid_Resource
 * @author   Nann Visal <visalnann@email.com>
 * @license  Zii23 Platform 
 * @link     http://
 */
class BidResource extends Resource
{
    
    protected static ?string $model = Bid::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'heroicon-m-square-3-stack-3d'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_bids'); // Individual resource label
    }


    /**
     * Undocumented function
     *
     * @param Form $form 
     * 
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_bid_title'))
                        ->schema(
                            [
                                TextInput::make('name')
                                    ->required()
                                    ->label(__('form_bid_name'))
                                    ->placeholder(__('form_bid_name'))
                                    ->maxValue(255),
                                TextInput::make('size')
                                    ->required()
                                    ->label(__('form_bid_size'))
                                    ->placeholder('0000.00 m2/m3')
                                    ->numeric(),
                                TextInput::make('budget')
                                    ->required()
                                    ->label(__('form_bid_budget'))
                                    ->placeholder('0000.00 $$')
                                    ->numeric(),
                                Textarea::make('description')
                                    ->required()
                                    ->rows(3)
                                    ->columnSpan(2)
                                    ->label(__('form_bid_description'))
                                    ->placeholder(__('form_bid_description'))
                                    ->autosize(),
                                Textarea::make('address')
                                    ->required()
                                    ->rows(3)
                                    ->label(__('form_bid_address'))
                                    ->placeholder(__('form_bid_address'))
                                    ->autosize(),
                                Select::make('types')
                                    ->options(
                                        [
                                            Types::REMODEL => __('form_bid_type_remodel'),
                                            Types::CREATENEW => __('form_bid_type_new_plan'),
                                            Types::PARTOF => __('form_bid_type_part_of'),
                                         ]
                                    )
                                    ->searchable()
                                    ->required()
                                    ->label(__('form_bid_type'))
                                    ->native(false),
                                Toggle::make('status')
                                    ->label(__('form_bid_status'))
                                    ->default(false)
                                    ->inline(false),
                                SpatieMediaLibraryFileUpload::make('document')
                                    ->label(__('form_bid_document'))
                                    ->collection('document')
                                    ->acceptedFileTypes(['application/pdf'])
                                    ->columnStart(1),
                                SpatieMediaLibraryFileUpload::make('thumbnail')
                                    ->label(__('form_bid_thumbnail'))
                                    ->collection('thumbnail')
                                    ->acceptedFileTypes(
                                        [
                                            'image/jpeg',
                                            'image/png',
                                            'image/svg+xml',
                                            'image/webp',
                                            'image/gif',
                                            'image/svg',
                                        ]
                                    ),
                            ]
                        )->columns(3)

                ]
            );
    }

    /**
     * Undocumented function
     *
     * @param Table $table use to render ui on web page
     * 
     * @return Table
     */
    public static function table(Table $table): Table
    {
        return $table
            ->paginated([10, 25, 50, 100, 'all'])
            ->columns(
                [
                    TextColumn::make('creator.name')
                        ->label(__('table_bid_creator'))
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('name')
                        ->label(__('table_bid_name'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('size')
                        ->label(__('table_bid_size'))
                        ->getStateUsing(
                            function (Bid $record) {
                                return $record->size . ' m2/m3';
                            }
                        )
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('budget')
                        ->label(__('table_bid_budget'))
                        ->getStateUsing(
                            function (Bid $record) {
                                return $record->budget . ' $$';
                            }
                        )
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('address')
                        ->label(__('table_bid_address'))
                        ->searchable()
                        ->sortable(),
                    ToggleColumn::make('status')
                        ->label(__('table_bid_status'))
                        ->afterStateUpdated(
                            function () {
                                Notification::make()
                                    ->title(__('notification_edit'))
                                    ->success() 
                                    ->send();
                            }
                        )
                        ->onColor('success')
                        ->sortable(),
                    TextColumn::make('types')
                        ->label(__('table_bid_type'))
                        ->getStateUsing(
                            function (Bid $record): string {
                                $type = $record->types;
                                switch ($type) {
                                case Types::REMODEL : 
                                    return __('table_bid_type_remodel');
                                case Types::CREATENEW :
                                    return __('table_bid_type_new_plan');
                                case Types::PARTOF :
                                    return __('table_bid_type_part_of');
                                }
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_bid_type_remodel') => 'gray',
                                __('table_bid_type_new_plan') => 'warning',
                                __('table_bid_type_part_of') => 'success',
                            }
                        )
                        ->sortable(),
                        
                ]
            )
            ->deferLoading()
            ->filters(
                [
                    Tables\Filters\TrashedFilter::make(),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                    Tables\Actions\DeleteAction::make()
                        ->label(__('button_delete'))
                        ->modalCancelActionLabel(__('button_cancel'))
                        ->modalHeading(__('button_delete_header'))
                        ->modalDescription(__('button_delete_description'))
                        ->modalSubmitActionLabel(__('button_delete_confirm'))
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                            Tables\Actions\DeleteBulkAction::make(),
                            Tables\Actions\ForceDeleteBulkAction::make(),
                            Tables\Actions\RestoreBulkAction::make(),
                        ]
                    ),
                ]
            );
    }
    /**
     * Undocumented function
     * 
     * @return array
     */
    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    /**
     * Undocumented function
     * 
     * @return array
     */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBids::route('/'),
            'create' => Pages\CreateBid::route('/create'),
            'edit' => Pages\EditBid::route('/{record}/edit'),
        ];
    }

    /**
     * Undocumented function
     *
     * @return Builder
     */
    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes(
                [
                    SoftDeletingScope::class,
                ]
            );
    }
}
