<?php

namespace App\Filament\Resources\ProjectTrackingResource\Pages;

use App\Filament\Resources\ProjectTrackingResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListProjectTrackings extends ListRecords
{
    protected static string $resource = ProjectTrackingResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->label(__('button_create')),
        ];
    }
}
