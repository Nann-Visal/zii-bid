<?php

namespace App\Filament\Resources;

use App\Models\Task;
use Filament\Tables;
use App\Models\Project;
use Filament\Forms\Form;
use Filament\Tables\Table;
use App\Models\ProjectTracking;
use Filament\Resources\Resource;
use Illuminate\Support\Collection;
use App\Enums\ProjectTracking\Status;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use App\Enums\Task\Status as TaskStatus;
use Filament\Forms\Components\TextInput;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\ProjectTrackingResource\Pages;

class ProjectTrackingResource extends Resource
{
    protected static ?string $model = ProjectTracking::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-tree-view-alt'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_project_track'); // Individual resource label
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_project_tracking_title'))
                        ->schema(
                            [
                                Select::make('project_id')
                                    ->label(__('form_project_tracking_project'))
                                    ->required()
                                    ->options(
                                        fn (): Collection => Project::query()
                                            ->whereNotIn(
                                                'id',
                                                ProjectTracking::pluck('project_id')
                                            )
                                            ->get()
                                            ->map(
                                                fn (Project $item) => [
                                                    'label' => $item->bidReviewed->bid ? $item->bidReviewed->bid->name : 'No record!',
                                                    'value' => $item->id,
                                                ]
                                            )
                                            ->pluck('label', 'value')
                                    )
                                    ->native(false)
                                    ->searchable()
                                    ->preload(),
                                TextInput::make('total_invest')
                                    ->label(__('form_project_tracking_total_invest'))
                                    ->required()
                                    ->numeric()
                                    ->placeholder('0000.00$$'),
                                TextInput::make('total_income')
                                    ->label(__('form_project_tracking_total_income'))
                                    ->numeric()
                                    ->placeholder('0000.00$$'),
                                Textarea::make('note')
                                    ->label(__('form_project_tracking_note'))
                                    ->required()
                                    ->placeholder(__('form_project_tracking_note').'. . .')
                                    ->rows(3)
                                    ->columnSpan(2),
                                Select::make('status')
                                    ->options(
                                        [
                                            Status::NEW_ARRIVAL => __('form_project_tracking_status_new_arrival'),
                                            Status::IN_PROGRESS => __('form_project_tracking_status_in_progress'),
                                            Status::IN_REVIEW => __('form_project_tracking_status_in_review'),
                                            Status::FAILS => __('form_project_tracking_status_fails'),
                                            Status::DONE => __('form_project_tracking_status_done'),
                                         ]
                                    )
                                    ->searchable()
                                    ->required()
                                    ->label(__('form_project_tracking_status'))
                                    ->native(false),        
                            ]
                        )->columns(3),
                ]
            );
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns(
                [
                    TextColumn::make('project.bidReviewed.bid.name')
                        ->label(__('table_project_tracking_project'))
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('total_invest')
                        ->label(__('table_project_tracking_total_invest'))
                        ->getStateUsing(
                            function (ProjectTracking $record) {
                                return $record->total_invest. ' $$';
                            }
                        )
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('total_income')
                        ->label(__('table_project_tracking_total_income'))
                        ->getStateUsing(
                            function (ProjectTracking $record) {
                                return $record->total_income. ' $$';
                            }
                        )
                        ->sortable()
                        ->searchable(),
                    TextColumn::make('status')
                        ->label(__('table_project_tracking_status'))
                        ->getStateUsing(
                            function (ProjectTracking $record): string {
                                $status = $record->status;
                                switch ($status) {
                                case  Status::NEW_ARRIVAL :
                                    return __('table_project_tracking_status_new_arrival');
                                case Status::IN_PROGRESS :
                                    return __('table_project_tracking_status_in_progress');
                                case Status::IN_REVIEW :
                                    return __('table_project_tracking_status_in_review');
                                case Status::FAILS :
                                    return __('table_project_tracking_status_fails');
                                case Status::DONE :
                                    return __('table_project_tracking_status_done');
                                }
                            }
                        )
                        ->badge()
                        ->color(
                            fn (string $state): string => match ($state) {
                                __('table_project_tracking_status_new_arrival') => 'gray',
                                __('table_project_tracking_status_in_progress') => 'info',
                                _('table_project_tracking_status_fails') => 'danger',
                                __('table_project_tracking_status_in_review') => 'primary',
                                __('table_project_tracking_status_done')  => 'success',
                            }
                        )
                        ->sortable(),
                    TextColumn::make('to_do_tasks')
                        ->label(__('table_project_tracking_todo_task'))
                        ->getStateUsing(
                            function ($record) {
                                $totals = Task::where(
                                    'project_id', $record->project_id
                                )->where('status', TaskStatus::TO_DO)
                                ->count();
                                return $totals;
                            }
                        )
                        ->badge()
                        ->color('gray'),
                    TextColumn::make('in_progress_tasks')
                        ->label(__('table_project_tracking_in_progress_task'))
                        ->getStateUsing(
                            function ($record) {
                                $totals = Task::where(
                                    'project_id', $record->project_id
                                )->where('status', TaskStatus::IN_PROGRESS)
                                ->count();
                                return $totals;
                            }
                        )
                        ->badge()
                        ->color('primary'),
                    TextColumn::make('in_problem_tasks')
                        ->label(__('table_project_tracking_in_problem_task'))
                        ->getStateUsing(
                            function ($record) {
                                $totals = Task::where(
                                    'project_id', $record->project_id
                                )->where('status', TaskStatus::IN_PROBLEM)
                                ->count();
                                return $totals;
                            }
                        )
                        ->badge()
                        ->color('danger'),
                    TextColumn::make('in_review_tasks')
                        ->label(__('table_project_tracking_in_review_task'))
                        ->getStateUsing(
                            function ($record) {
                                $totals = Task::where(
                                    'project_id', $record->project_id
                                )->where('status', TaskStatus::IN_REVIEW)
                                ->count();
                                return $totals;
                            }
                        )
                        ->badge()
                        ->color('info'),
                    TextColumn::make('in_done_tasks')
                        ->label(__('table_project_tracking_in_done_task'))
                        ->getStateUsing(
                            function ($record) {
                                $totals = Task::where(
                                    'project_id', $record->project_id
                                )->where('status', TaskStatus::DONE)
                                ->count();
                                return $totals;
                            }
                        )
                        ->badge()
                        ->color('success'),
                ]
            )
            ->filters(
                [
                    Tables\Filters\TrashedFilter::make(),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                    Tables\Actions\DeleteAction::make()
                        ->label(__('button_delete'))
                        ->modalCancelActionLabel(__('button_cancel'))
                        ->modalHeading(__('button_delete_header'))
                        ->modalDescription(__('button_delete_description'))
                        ->modalSubmitActionLabel(__('button_delete_confirm'))
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                            Tables\Actions\DeleteBulkAction::make(),
                            Tables\Actions\ForceDeleteBulkAction::make(),
                            Tables\Actions\RestoreBulkAction::make(),
                        ]
                    ),
                ]
            );
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProjectTrackings::route('/'),
            'create' => Pages\CreateProjectTracking::route('/create'),
            'edit' => Pages\EditProjectTracking::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
