<?php

namespace App\Filament\Resources;

use App\Models\User;
use Filament\Tables;
use Filament\Forms\Form;
use App\Enums\User\Types;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Tables\Filters\Filter;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Notifications\Notification;
use Filament\Tables\Columns\ImageColumn;
use Filament\Forms\Components\DatePicker;
use Filament\Tables\Columns\ToggleColumn;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\UserResource\Pages;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'heroicon-o-users'; // Use Filament's icon names
    }

    /**
     * Overrides static function label
     *
     * @return string
     */
    public static function getLabel(): string
    {
        return __('nav_title_user'); // Individual resource label
    }
    
    public static function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_user_title'))
                        ->schema(
                            [
                                    TextInput::make('name')
                                        ->required()
                                        ->label(__('form_user_full_name'))
                                        ->placeholder(__('form_user_full_name').'. . .')
                                        ->maxValue(255),
                                    TextInput::make('email')
                                        ->required()
                                        ->label(__('form_user_email'))
                                        ->placeholder(__('form_user_email').'. . .')
                                        ->email()
                                        ->autocomplete(false)
                                        ->maxValue(255)
                                        ->unique(ignoreRecord: true),
                                    TextInput::make('phone')
                                        ->required()
                                        ->label(__('form_user_phone_number'))
                                        ->tel()
                                        ->placeholder(__('form_user_phone_number').'. . .')
                                        ->maxValue(25)
                                        ->unique(ignoreRecord: true),
                                    Textarea::make('address')
                                        ->required()
                                        ->label(__('form_user_current_address'))
                                        ->placeholder(__('form_user_current_address').'. . .')
                                        ->autosize()
                                        ->rows(3),
                                    Textarea::make('billing_address')
                                        ->required()
                                        ->label(__('form_user_permanent_address'))
                                        ->placeholder(__('form_user_permanent_address').'. . .')
                                        ->autosize()
                                        ->rows(3)
                                        ->columnSpan(2),
                                    DatePicker::make('date_of_birth')
                                        ->required()
                                        ->label(__('form_user_date_of_birth'))
                                        ->placeholder('dd-mm-YY')
                                        ->displayFormat('d-m-Y')
                                        ->closeOnDateSelection()
                                        ->native(false),
                                    Select::make('types')
                                        ->options(
                                            [
                                                Types::CONSTRUCTOR => __('form_user_type_constructor'),
                                                Types::HOMEOWNER => __('form_user_type_home_owner'),
                                            ]
                                        )
                                        ->searchable()
                                        ->required()
                                        ->label(__('form_user_type'))
                                        ->native(false),
                                    Toggle::make('status')
                                        ->label(__('form_user_status'))
                                        ->default(true)
                                        ->inline(false),
                                    
                                    TextInput::make('password')
                                        ->visibleOn(['create'])
                                        ->autocomplete(false)
                                        ->required()
                                        ->password()
                                        ->revealable()
                                        ->label(__('form_user_password'))
                                        ->placeholder(__('form_user_password').'. . .'),
                                    TextInput::make('password_confirmation')
                                        ->visibleOn(['create'])
                                        ->same('password')
                                        ->required()
                                        ->password()
                                        ->revealable()
                                        ->label(__('form_user_password_confirm'))
                                        ->placeholder(__('form_user_password_confirm').'. . .'),
                                    SpatieMediaLibraryFileUpload::make('avatar')
                                        ->label(__('form_user_avatar'))
                                        ->collection('avatars')
                                        ->responsiveImages(),
                                    Select::make('roles')
                                        ->label(__('form_user_role'))        
                                        ->relationship('roles', 'name')

                                ]
                        )
                        ->columns(3),
                ]
            );
    }

    public static function table(Table $table): Table
    {
        return $table
            ->paginated([10, 25, 50, 100, 'all'])
            ->columns(
                [
                    ImageColumn::make('avatar')
                        ->label(__('table_user_avatar'))
                        ->size(60)
                        ->getStateUsing(
                            function ($record) {
                                return $record->getFirstMediaUrl('avatars');
                            }
                        )
                        ->circular(),
                    TextColumn::make('name')
                        ->label(__('table_user_name'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('email')
                        ->label(__('table_user_email'))
                        ->searchable()
                        ->sortable(),
                    TextColumn::make('phone')
                        ->label(__('table_user_phone'))
                        ->searchable()
                        ->sortable(),
                    ToggleColumn::make('status')
                        ->label(__('table_user_status'))
                        ->afterStateUpdated(
                            function () {
                                Notification::make()
                                    ->title(__('notification_edit'))
                                    ->success() 
                                    ->send();
                            }
                        )
                        ->onColor('success'),
                    TextColumn::make('types')
                        ->label(__('table_user_type'))
                        ->getStateUsing(
                            function (User $record): string {
                                $type = $record->types;
                                return $type == 0 ? __('table_user_type_constructor') : __('table_user_type_home_owner');
                            }
                        )
                        ->badge()
                        ->color('primary')
                        ->sortable(),
                ]
            )
            ->deferLoading()
            ->filters(
                [
                    Filter::make('name')
                        ->query(fn (Builder $query) => $query->orderBy('name')),
                    SelectFilter::make('types')
                        ->options(
                            [
                                0 => __('table_user_type_constructor'),
                                1 => __('table_user_type_home_owner'),
                            ]
                        ),
                ]
            )
            ->actions(
                [
                    Tables\Actions\ViewAction::make()
                        ->label(__('button_view'))
                        ->modalHeading(__('button_view')),
                    Tables\Actions\EditAction::make()
                        ->label(__('button_edit')),
                ]
            )
            ->bulkActions(
                [
                    Tables\Actions\BulkActionGroup::make(
                        [
                            Tables\Actions\DeleteBulkAction::make(),
                        ]
                    ),
                ]
            );
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }

}
