<?php

namespace App\Filament\Pages;

use Filament\Forms\Form;
use Filament\Actions\Action;
use App\Settings\GeneralSetting;
use Filament\Pages\SettingsPage;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\KeyValue;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\RichEditor;

class Setting extends SettingsPage
{

    protected static string $settings = GeneralSetting::class;

    /**
     * Overrides static function icon
     *
     * @return string
     */
    public static function getNavigationIcon(): string
    {
        return 'carbon-settings'; // Use Filament's icon names
    }

    public static function getNavigationLabel(): string
    {
        return  __('nav_title_setting');
    }

    /**
     * Undocumented function
     *
     * @param Form $form uncomment
     * 
     * @return Form
     */
    public function form(Form $form): Form
    {
        return $form
            ->schema(
                [
                    Section::make(__('form_setting_company_infos'))
                        ->schema(
                            [
                                TextInput::make('company_name')
                                    ->label(__('form_setting_company_name'))
                                    ->required()
                                    ->maxValue(255),
                                TextInput::make('phone')
                                    ->label(__('form_setting_company_phone_number'))
                                    ->required()
                                    ->maxValue(50),
                                TextInput::make('email')
                                    ->label(__('form_setting_company_email'))
                                    ->required()
                                    ->email(),
                                TextInput::make('copy_right_text')
                                    ->label(__('form_setting_company_copy_right_text')),
                                Textarea::make('company_address')
                                    ->label(__('form_setting_company_address'))
                                    ->required()
                                    ->rows(1)
                                    ->columnSpan(2)
                                 
                            ]
                        )->columns(3),
                    Section::make(__('form_setting_company_social_media'))
                        ->schema(
                            [
                                KeyValue::make('social_medias')
                                    ->label('')
                                    ->keyLabel(__('form_setting_company_social_medias_key'))
                                    ->valueLabel(__('form_setting_company_social_medias_value'))
                                    ->addActionLabel(__('button_create'))
                                    ->columnSpan(4),
                                
                            ]
                        )->columns(4),
                    Section::make(__('form_setting_company_about_us'))
                        ->schema(
                            [
                                RichEditor::make('about_us')
                                    ->label('')
                                    ->required()
                            ]
                        )->columnSpan(4),
                    Section::make(__('form_setting_company_privacy_and_policy'))
                        ->schema(
                            [
                                RichEditor::make('privacy_policy')
                                    ->label('')
                                    ->required()
                                    ->required()
                            ]
                        )->columnSpan(4),
                ]
            )->columns(4);
    }


    /**
     * Custom label on button save
     *
     * @return Action
     */
    public function getSaveFormAction(): Action
    {
        return parent::getSaveFormAction()
            ->label(__('button_save_edit'));
    }

}
