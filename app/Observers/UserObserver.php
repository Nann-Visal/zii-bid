<?php

namespace App\Observers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;

class UserObserver
{
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        //check user request from which panel between homeowner and constructor 
        $sources_of_request = request()->header('referer');
        if (Str::contains($sources_of_request, 'app-constructor')) {
            $user->assignRole(Role::CONSTRUCTOR);
        } else if (Str::contains($sources_of_request, 'app-homeowner')) {
            $user->assignRole(Role::HOME_OWNER);
        }
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        //
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
