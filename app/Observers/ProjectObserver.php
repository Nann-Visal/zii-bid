<?php

namespace App\Observers;

use App\Enums\Project\Status;
use App\Models\BidReview;
use App\Models\Project;

class ProjectObserver
{
    public function created(BidReview $bidReview)
    {
        //auto create project after bid has been reviewed
        if(isset($bidReview) && (!$bidReview->project || isset($bidReview->project->deleted_at)))
        {
            $project = new Project();
            
            $project->budget = $bidReview->bid->budget;
            $project->size = $bidReview->bid->size;
            $project->location = $bidReview->bid->address;
            $project->status = Status::ACTIVE;
            $project->types = $bidReview->bid->types;
            $project->bid_review_id = $bidReview->id;
            $project->created_by = $bidReview->bidItem->created_by;

            $project->save();
        }
    }
}
