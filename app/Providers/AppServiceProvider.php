<?php

namespace App\Providers;

use App\Models\User;
use App\Models\BidReview;
use App\Observers\UserObserver;
use App\Observers\ProjectObserver;
use Illuminate\Support\ServiceProvider;
use BezhanSalleh\FilamentLanguageSwitch\LanguageSwitch;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        BidReview::observe(ProjectObserver::class);
        User::observe(UserObserver::class);

        /**
         * This code use to define Who can make translate key for multi languages?
         */
        LanguageSwitch::configureUsing(
            function (LanguageSwitch $switch) {
                $switch
                    ->locales(
                        [
                        'en','kh'
                        ]
                    )
                    ->labels(
                        [
                            'kh' => __('locale_kh'),
                            'en' => __('locale_en')
                        ]
                    )
                    ->circular()
                    ->flags(
                        [
                            'kh' => asset('flags/kh.svg'),
                            'en' => asset('flags/sh.svg'),
                        ]
                    ); // also accepts a closure
            }
        );
    }

    
}
