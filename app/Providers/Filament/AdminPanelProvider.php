<?php

namespace App\Providers\Filament;

use Filament\Panel;
use Filament\PanelProvider;
use App\Filament\Pages\Setting;
use Filament\Support\Colors\Color;
use Filament\Support\Enums\MaxWidth;
use App\Filament\Resources\BidResource;
use App\Filament\Resources\RoleResource;
use App\Filament\Resources\TaskResource;
use App\Filament\Resources\UserResource;
use Filament\Navigation\NavigationGroup;
use Filament\Http\Middleware\Authenticate;
use Filament\Navigation\NavigationBuilder;
use App\Filament\Resources\BidItemResource;
use App\Filament\Resources\MessageResource;
use App\Filament\Resources\ProjectResource;
use App\Filament\Resources\BidReviewResource;
use Filament\SpatieLaravelTranslatablePlugin;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Cookie\Middleware\EncryptCookies;
use App\Filament\Resources\ProjectTrackingResource;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Althinect\FilamentSpatieRolesPermissions\Resources\PermissionResource;
use Althinect\FilamentSpatieRolesPermissions\FilamentSpatieRolesPermissionsPlugin;

class AdminPanelProvider extends PanelProvider
{
    /**
     * Undocumented function
     *
     * @param Panel $panel uncomment
     * 
     * @return Panel
     */
    public function panel(Panel $panel): Panel
    {
        return $panel
            ->default()
            ->id('admin')
            ->path('admin')
            ->login()
            ->colors(
                [
                    'danger' => Color::Rose,
                    'gray' => Color::Gray,
                    'info' => Color::Blue,
                    'primary' => Color::Indigo,
                    'success' => Color::Emerald,
                    'warning' => Color::Orange,
                    'blue' => Color::Blue,
                    'red' => Color::Red,
                ]
            )
            ->spa()
            ->brandName('Zii-23')
            ->sidebarCollapsibleOnDesktop()
            ->breadcrumbs(false)
            ->maxContentWidth(MaxWidth::Full)
            ->discoverResources(in: app_path('Filament/Resources'), for: 'App\\Filament\\Resources')
            ->discoverPages(in: app_path('Filament/Pages'), for: 'App\\Filament\\Pages')
            ->discoverWidgets(in: app_path('Filament/Widgets'), for: 'App\\Filament\\Widgets')
            ->widgets(
                [
                ]
            )
            ->databaseNotifications()
            ->databaseNotificationsPolling('30s')
            ->registration()
            ->passwordReset()
            ->emailVerification()
            ->profile()
            ->navigation(
                function (NavigationBuilder $builder): NavigationBuilder {
                    return $builder->groups(
                        [
                            NavigationGroup::make(__('nav_group_bid'))
                                ->items(
                                    [
                                    ...BidResource::getNavigationItems(),
                                    ...BidItemResource::getNavigationItems(),
                                    ...BidReviewResource::getNavigationItems(),
                                    ]
                                ),
                            NavigationGroup::make(__('nav_group_project'))
                                ->items(
                                    [
                                    ...ProjectResource::getNavigationItems(),
                                    ...ProjectTrackingResource::getNavigationItems(),
                                    ...TaskResource::getNavigationItems(),
                                    ]
                                ),
                            NavigationGroup::make(__('nav_group_collaboration'))
                                ->items(
                                    [
                                    ...MessageResource::getNavigationItems(),
                                    ]
                                ),
                            NavigationGroup::make(__('nav_group_users'))
                                ->items(
                                    [
                                    ...UserResource::getNavigationItems(),
                                    ...RoleResource::getNavigationItems(),
                                    ...PermissionResource::getNavigationItems(),
                                    ]
                                ),
                            NavigationGroup::make(__('nav_group_setting'))
                                ->items(
                                    [
                                    ...Setting::getNavigationItems(),
                                    ]
                                ),
                        ]
                    );
                }
            )
            ->middleware(
                [
                    EncryptCookies::class,
                    AddQueuedCookiesToResponse::class,
                    StartSession::class,
                    AuthenticateSession::class,
                    ShareErrorsFromSession::class,
                    VerifyCsrfToken::class,
                    SubstituteBindings::class,
                    DisableBladeIconComponents::class,
                    DispatchServingFilamentEvent::class,
                ]
            )
            ->authMiddleware(
                [
                    Authenticate::class,
                ]
            )
            ->plugins(
                [
                    FilamentSpatieRolesPermissionsPlugin::make(),
                    SpatieLaravelTranslatablePlugin::make()
                        ->defaultLocales(['en', 'kh'])
                ]
            );
    }
}
