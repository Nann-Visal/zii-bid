<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.site_name', 'Zii-Bid');
        $this->migrator->add('general.about_us', 'About us');
        $this->migrator->add('general.privacy_policy', 'Privacy Policy Page');
        $this->migrator->add(
            'general.languages', 
            [
                [
                    'name' => 'English',
                    'code' => 'en',
                    'status' => 1,
                    'default' => true,
                ]
            ]
        );
        $this->migrator->add('general.copy_right_text', 'Copyright Text');
        $this->migrator->add(
            'general.social_medias', 
            [
                'facebook' => 'https://www.facebook.com/SalUndro',
                'linked_in' => 'www.linkedin.com/in/nionacy',
                'telegram' => 'https://t.me/Nann_Visal',
                'laracasts' => 'https://laracasts.com/@NannVisal',
            ]
        );
        $this->migrator->add('general.company_name', 'Zii-23');
        $this->migrator->add('general.phone', '+855-996699XX');
        $this->migrator->add('general.email', 'example@zii23gmail.io.com');
        $this->migrator->add('general.company_address', 'Cambodia, Phnom Penh');

    }
};
