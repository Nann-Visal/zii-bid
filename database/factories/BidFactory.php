<?php

namespace Database\Factories;

use App\Models\Bid;
use App\Models\User;
use App\Enums\Bid\Types;
use App\Enums\Bid\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class BidFactory extends Factory
{
    protected $model = Bid::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'size' => fake()->randomFloat(2, 0, 10000),
            'budget' => fake()->randomFloat(2, 0, 10000),
            'description' => fake()->text(500),
            'address' => fake()->streetAddress(),
            'status' => Status::getRandomValue(),
            'types' => Types::getRandomValue(),
            'created_by' => User::inRandomOrder()->first()->id,
        ];
    }
}
