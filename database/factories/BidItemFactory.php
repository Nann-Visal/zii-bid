<?php

namespace Database\Factories;

use App\Models\Bid;
use App\Models\BidItem;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BidItem>
 */
class BidItemFactory extends Factory
{
    protected $model = BidItem::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'bid_taxes' => fake()->randomFloat(2, 0, 10000),
            'description' => fake()->text(300),
            'bid_id' => Bid::inRandomOrder()->first()->id,
            'created_by' => User::inRandomOrder()->first()->id,
        ];
    }
}
