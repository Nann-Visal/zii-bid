<?php

use App\Enums\Bid\Types;
use App\Enums\Bid\Status;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->decimal('size',10,4)->nullable();
            $table->decimal('budget',10,4)->nullable();
            $table->text('description');
            $table->text('address');
            $table->timestamp('expired_at')->default(Carbon::now()->addDay(7));
            $table->tinyInteger('status')->default(Status::OPEN);
            $table->tinyInteger('types')->default(Types::CREATENEW);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bids');
    }
};
