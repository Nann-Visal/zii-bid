<?php

use App\Enums\ProjectTracking\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('project_trackings', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_invest', 10, 4)->default(0);
            $table->decimal('total_income', 10, 4)->nullable()->default(0);
            $table->text('note')->nullable();
            $table->tinyInteger('status')->default(Status::NEW_ARRIVAL);
            $table->foreignId('project_id')->nullable()->constrained('projects')->unique();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('project_trackings');
    }
};
