<?php

namespace Database\Seeders;

use App\Models\BidItem;
use Illuminate\Database\Seeder;
class BidItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BidItem::factory(1)->create();
    }
}
