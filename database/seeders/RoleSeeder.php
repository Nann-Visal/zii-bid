<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = Role::allRoles();

        foreach ($roles as $role) {
            $role = Role::firstOrCreate(
                [
                    'name' => trim($role), 
                    'guard_name' => 'web'
                ]
            );

            switch ($role->name) {
                case Role::ADMIN:
                    $role->syncPermissions(Permission::where('guard_name', 'web')->get());
                    break;
            }
        }
    }
}
