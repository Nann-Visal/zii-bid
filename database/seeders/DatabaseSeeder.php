<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Enums\User\Types;
use Database\Seeders\BidSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => Hash::make('password'),
            'phone' => '098765432',
            'address' => 'Test address',
            'billing_address' => 'Test billing address',
            'date_of_birth' => fake()->date(),
            'types' => Types::getRandomValue(),
        ]);

        $this->call(
            [
                BidSeeder::class,
                BidItemSeeder::class,
                RoleSeeder::class,
            ]
        );
    }
}
    